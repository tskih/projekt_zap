QT       += core gui opengl openglwidgets
LIBS += -lopengl32 -lglu32
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    drugie_okno.cpp \
    main.cpp \
    mainwindow.cpp \
    oglwidget.cpp

HEADERS += \
    drugie_okno.h \
    mainwindow.h \
    oglwidget.h \
    parse.h \
    polar_to_cartesian.h \
    vector_map.h

FORMS += \
    drugie_okno.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
