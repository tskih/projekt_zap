#include "drugie_okno.h"
#include "oglwidget.h"
#include "ui_drugie_okno.h"
#include<fstream>

extern std::string file_name;

drugie_okno::drugie_okno(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::drugie_okno)
{

    ui->setupUi(this);

    //laczenie sygnalow zmiany xyz z widget-em
    connect(ui->owidget, SIGNAL(xRotationChanged(int)), ui->rotXSlider, SLOT(setValue(int)));
    connect(ui->owidget, SIGNAL(zRotationChanged(int)), ui->rotZSlider, SLOT(setValue(int)));
    connect(ui->owidget, SIGNAL(yRotationChanged(int)), ui->rotYSlider, SLOT(setValue(int)));


}


drugie_okno::~drugie_okno()
{

    delete ui;
}



void drugie_okno::on_exit_button_clicked()
{
    this->close();
}


void drugie_okno::on_checkBox_toggled(bool checked)
{
    if(checked){
      ui->rotXSlider->hide();
      ui->rotYSlider->hide();
      ui->rotZSlider->hide();
      ui->label_x->hide();
      ui->label_y->hide();
      ui->label_z->hide();

    }

    else{
        ui->rotXSlider->show();
        ui->rotYSlider->show();
        ui->rotZSlider->show();
        ui->label_x->show();
        ui->label_y->show();
        ui->label_z->show();


    }
}

