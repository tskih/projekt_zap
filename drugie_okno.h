#ifndef DRUGIE_OKNO_H
#define DRUGIE_OKNO_H

#include <QDialog>

namespace Ui {
class drugie_okno;
}

class drugie_okno : public QDialog
{
    Q_OBJECT

public:
    explicit drugie_okno(QWidget *parent = nullptr);
    ~drugie_okno();

private slots:
    void on_exit_button_clicked();

    void on_checkBox_toggled(bool checked);

private:
    Ui::drugie_okno *ui;

};

#endif // DRUGIE_OKNO_H
