#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "drugie_okno.h"

#include<QMessageBox>
#include<QFileDialog>


std::string file_name;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()    //zamknij program
{
    this->close();
}


void MainWindow::on_pushButton_2_clicked()  //example 2d
{
    drugie_okno mode_example;
    file_name="C:/example/polar2d.txt";
    mode_example.setModal(true);
    mode_example.exec();
}


void MainWindow::on_pushButton_3_clicked()  //example 3d
{
drugie_okno mode_example;
file_name="C:/example/polar3d.txt";
mode_example.setModal(true);
mode_example.exec();
}


void MainWindow::on_pushButton_4_clicked()  //z pliku
{
    QString file=QFileDialog::getOpenFileName(this, "wybierz plik zrodlowy", "C://", tr("plik tekstowy (*.txt)"));
    QMessageBox::information(this,"wybrano", file);
    file_name=file.toStdString();

    drugie_okno mode_file;
    mode_file.setModal(true);
    mode_file.exec();
}
