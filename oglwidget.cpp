#include "oglwidget.h"
#include<vector_map.h>
#include "qevent.h"
#include<iostream>

extern string file_name;


OGLWidget::OGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
      xRot = 0;
      yRot = 0;
      zRot = 0;
}


OGLWidget::~OGLWidget()
{
}


//testowanie ilosci wymiarow
int dimension_num(string file_name){

    string in_string;
    ifstream cartesian_file;
    int length=0;
    cartesian_file.open(file_name.c_str());
    getline(cartesian_file, in_string);
    cartesian_file.close();
    for(unsigned int i=0; i<in_string.size(); i++)
        {
            if(in_string[i]==' ')
                length++;
        }
    return length+1;
}
int d_num=dimension_num("cartesian.txt");


void OGLWidget::setXRotation(int angle)
{
    if (angle != xRot) {
        xRot = angle;
        emit xRotationChanged(angle);
        update();
    }
}

void OGLWidget::setYRotation(int angle)
{
    if (angle != yRot) {
        yRot = angle;
        emit yRotationChanged(angle);
        update();
    }
}

void OGLWidget::setZRotation(int angle)
{
    if (angle != zRot) {
        zRot = angle;
        emit zRotationChanged(angle);
        update();
    }
}

void OGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void OGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();
    int sensitivity=10;

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + sensitivity * dy);
        setYRotation(yRot + sensitivity * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + sensitivity * dy);
        setZRotation(zRot + sensitivity * dx);
    }

    lastPos = event->pos();
}



void OGLWidget::initializeGL()
{
    CREATE_FILE_polar_to_cartesian(file_name, "cartesian.txt");

    d_num=dimension_num("cartesian.txt");

    if(d_num==3)
    {
        CREATE_FILE_vector_map("cartesian.txt", "vector_map.txt");
        //ustawienia oswietlenia, renderowania w przypadku modelu 3d
        glShadeModel(GL_SMOOTH);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        static GLfloat lightPosition[4] = { 10,10, 10, 1.0 };
        glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

        //ustawienia renderowania modelu 3d
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
    }
    resizeGL(this->width(), this->height());

}

void OGLWidget::paintGL()
{

            string temp, dim_test;
            ifstream cartesian_file;
            ifstream plik_wektory;

            float* xyz;
            float wektor1[3];
            float wektor2[3];
            float wektor3[3];
            float** punkty = nullptr;
            int max_plane=0;
            int max_plane_temp=0;


            float plane[3][3];
            float normal[3];
            float AB[3];
            float AC[3];
            bool first_loop=true;
            bool cond=true;
            int ilosc_wektorow=3;

            // generowanie obrazu
            cartesian_file.open("cartesian.txt");
            d_num=dimension_num("cartesian.txt");

            //dla 2 wymiarow
            if(d_num==2){
                glClear(GL_COLOR_BUFFER_BIT);
                glBegin(GL_POLYGON);

                getline(cartesian_file, temp);
                while(temp.length()){
                    if(cartesian_file.eof()) break;
                    xyz=parse_xyz(temp,2);
                    glVertex2f((xyz[0]), (xyz[1]));
                    getline(cartesian_file, temp);
                }
                glEnd();
            }

            //dla 3 wymiarow

            else if(d_num==3)
            {


                tab_create2d(punkty, ilosc_linii("cartesian.txt"), 3);
                tab_fill2d(punkty, ilosc_linii("cartesian.txt"), 3, "cartesian.txt");



                for(int pierwszy_punkt=0; pierwszy_punkt<ilosc_linii("cartesian.txt"); pierwszy_punkt++){
                    for(int drugi_punkt=(1+pierwszy_punkt); drugi_punkt<ilosc_linii("cartesian.txt");drugi_punkt++){
                        for(int trzeci_punkt=(1+drugi_punkt); trzeci_punkt<ilosc_linii("cartesian.txt");trzeci_punkt++){
                            for(int czwarty_punkt=(1+trzeci_punkt); czwarty_punkt<ilosc_linii("cartesian.txt");czwarty_punkt++){
                                //std::cout<<"[P]: "<<punkty[pierwszy_punkt][0]<<" : "<<punkty[pierwszy_punkt][1]<<" : "<<punkty[pierwszy_punkt][2]<<std::endl;
                                //std::cout<<"[D]: "<<punkty[drugi_punkt][0]<<" : "<<punkty[drugi_punkt][1]<<" : "<<punkty[drugi_punkt][2]<<std::endl;
                                //std::cout<<"[T]: "<<punkty[trzeci_punkt][0]<<" : "<<punkty[trzeci_punkt][1]<<" : "<<punkty[trzeci_punkt][2]<<std::endl;
                                //std::cout<<"[C]: "<<punkty[czwarty_punkt][0]<<" : "<<punkty[czwarty_punkt][1]<<" : "<<punkty[czwarty_punkt][2]<<std::endl<<std::endl;
                                    for(int n=0; n<3; n++){
                                        //std::cout<<"[P]: "<<punkty[pierwszy_punkt][n]<<" : "<<punkty[pierwszy_punkt][1]<<" : "<<punkty[pierwszy_punkt][2]<<std::endl;
                                        //std::cout<<"[D]: "<<punkty[drugi_punkt][0]<<" : "<<punkty[drugi_punkt][1]<<" : "<<punkty[drugi_punkt][2]<<std::endl;
                                        //std::cout<<"[T]: "<<punkty[pierwszy_punkt][0]<<" : "<<punkty[pierwszy_punkt][1]<<" : "<<punkty[pierwszy_punkt][2]<<std::endl;

                                        wektor1[n]=Vector(punkty[pierwszy_punkt][n], punkty[drugi_punkt][n]);
                                        wektor2[n]=Vector(punkty[pierwszy_punkt][n], punkty[trzeci_punkt][n]);
                                        wektor3[n]=Vector(punkty[pierwszy_punkt][n], punkty[czwarty_punkt][n]);
                                        //std::cout<<"[P]: "<<"x"<<wektor1[0]<<"  y"<<wektor1[1]<<"  z"<<wektor1[2]<<std::endl;
                                        //std::cout<<"[D]: "<<"x"<<wektor2[0]<<"  y"<<wektor2[1]<<"  z"<<wektor2[2]<<std::endl;
                                        //std::cout<<"[T]: "<<"x"<<wektor3[0]<<"  y"<<wektor3[1]<<"  z"<<wektor3[2]<<std::endl<<std::endl;;
                                    }
                                    if(coplanar(wektor1, wektor2, wektor3)){
                                    max_plane_temp++;
                                    }
                            }
                            if(max_plane_temp>max_plane)
                                max_plane=max_plane_temp;
                        }
                    }
                }
                std::cout<<"mp "<<max_plane;


                for(int pierwszy_punkt=0; pierwszy_punkt<ilosc_linii("cartesian.txt"); pierwszy_punkt++){
                    for(int drugi_punkt=(1+pierwszy_punkt); drugi_punkt<ilosc_linii("cartesian.txt");drugi_punkt++){
                        for(int trzeci_punkt=(1+drugi_punkt); trzeci_punkt<ilosc_linii("cartesian.txt");trzeci_punkt++){
                            for(int czwarty_punkt=(1+trzeci_punkt); czwarty_punkt<ilosc_linii("cartesian.txt");czwarty_punkt++){

                            }
                        }
                    }
                }


/*
                {
                    int rowCount=ilosc_wektorow("vector_map.txt");
                    int colCount=3;
                    float** wektory = new float*[rowCount];
                    for(int i = 0; i < rowCount; ++i)
                        wektory[i] = new float[colCount];
                }

                glLoadIdentity();
                glTranslatef(0.0, 0.0, -10.0);
                glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
                glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
                glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);
                glBegin(GL_POLYGON);

                getline(cartesian_file, temp);

                while(temp.length()){
                    if(cartesian_file.eof()) break;


                    do{

                        xyz=parse_xyz(temp,3);
                        wektory[n][0]=xyz[0];
                        wektory[n][1]=xyz[1];
                        wektory[n][2]=xyz[2];
                        n++;
                        coplanar(wektory[n], wektory[n+1], wektory[n+2]);
                                getline(cartesian_file, temp);
                    }

                //cartesian_file.close();
                //cartesian_file.open("cartesian.txt");




                    if(first_loop){
                        for(int i=0; i<3; i++)
                        {
                            xyz=parse_xyz(temp,3);
                            //dla kazdego z 3 pkt
                            plane[i][0]=xyz[0]; //x
                            plane[i][1]=xyz[1]; //y
                            plane[i][2]=xyz[2]; //z
                            getline(cartesian_file, temp);
                            if(cartesian_file.eof()) break;

                        }
                        AB[0]=Vector(plane[0][0], plane[1][0]); //skladowa x wektora AB
                        AB[1]=Vector(plane[0][1], plane[1][1]); //skladowa y wektora AB
                        AB[2]=Vector(plane[0][2], plane[1][2]); //skladowa z wektora AB

                        AC[0]=Vector(plane[0][0], plane[2][0]); //skladowa x wektora AC
                        AC[1]=Vector(plane[0][1], plane[2][1]); //skladowa y wektora AC
                        AC[2]=Vector(plane[0][2], plane[2][2]); //skladowa z wektora AC

                        normal[0]=AB[1]*AC[2]-AB[2]*AC[1];//skladowa x wektora normalnego
                        normal[1]=AB[2]*AC[0]-AB[0]*AC[2];//skladowa y wektora normalnego
                        normal[2]=AB[0]*AC[1]-AB[1]*AC[0];//skladowa z wektora normalnego

                        glNormal3f(normal[0], normal[1], normal[2]);
                        glVertex3f((plane[0][0]), (plane[0][1]), (plane[0][2]));
                        glVertex3f((plane[1][0]), (plane[1][1]), (plane[1][2]));
                        glVertex3f((plane[2][0]), (plane[2][1]), (plane[2][2]));
                        first_loop=false;
                    }

                    std::cout<<temp<<endl;
                    xyz=parse_xyz(temp,3);
                    plane[0][0]=xyz[0]; //x
                    plane[0][1]=xyz[1]; //y
                    plane[0][2]=xyz[2]; //z
                    //std::cout<<xyz[0]<<" "<<xyz[1]<<" "<<xyz[2]<<endl;
                    AB[0]=Vector(plane[0][0], plane[1][0]); //skladowa x nowego wektora
                    AB[1]=Vector(plane[0][1], plane[1][1]); //skladowa y nowego wektora
                    AB[2]=Vector(plane[0][2], plane[1][2]); //skladowa z nowego wektora

                    AC[0]=Vector(plane[0][0], plane[2][0]); //skladowa x nowego wektora
                    AC[1]=Vector(plane[0][1], plane[2][1]); //skladowa y nowego wektora
                    AC[2]=Vector(plane[0][2], plane[2][2]); //skladowa z nowego wektora

                    normal[0]=AB[1]*AC[2]-AB[2]*AC[1];//skladowa x nowego wektora normalnego
                    normal[1]=AB[2]*AC[0]-AB[0]*AC[2];//skladowa y nowego wektora normalnego
                    normal[2]=AB[0]*AC[1]-AB[1]*AC[0];//skladowa z nowego wektora normalnego

                    //glNormal3f(normal[0], normal[1], normal[2]);
                    glVertex3f((plane[0][0]), (plane[0][1]), (plane[0][2]));
                    glVertex3f((plane[1][0]), (plane[1][1]), (plane[1][2]));
                    glVertex3f((plane[2][0]), (plane[2][1]), (plane[2][2]));
                    getline(cartesian_file, temp);
                    if(cartesian_file.eof()) break;

              }


*/            glEnd();
                tab_delete2d(punkty, ilosc_linii("cartesian.txt"));



glFlush();
            }}

void OGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    qreal aspectratio=qreal(w)/qreal(h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    d_num=dimension_num("cartesian.txt");


    //zmiana perspektywy ze wzgledu na ilosc wymiarow
    if(d_num==2){
       glOrtho(-1*aspectratio, 1*aspectratio, -1,1,1,-1);
    }
    else if(d_num==3)
    {
    gluPerspective(75, aspectratio, 0.1, 400000000);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    }

}
