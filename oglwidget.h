#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <QOpenGLWidget>
#include <gl/GLU.h>
#include"drugie_okno.h"

class OGLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    OGLWidget(QWidget *parent=nullptr);
    ~OGLWidget();

private:
    int xRot;
    int yRot;
    int zRot;
    QPoint lastPos;

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);


public slots:
    // sloty na obrot xyz w 3d
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);

signals:
    // sygnaly obrotu xyz z myszki
    void xRotationChanged(int angle);
    void yRotationChanged(int angle);
    void zRotationChanged(int angle);


};

#endif // OGLWIDGET_H
