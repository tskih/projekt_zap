//wyciaganie danych z wierszy plikow txt

#include<string>
#include<iostream>
using namespace std;

//wyciaganie kata 'xy' z wiersza pliku polar.txt
float parse_angle(string input){
        string temp;
        int i=0;

        while(input[i]!=' ')
            {
                temp+=input[i];
                i++;
            }

        return stof(temp);
    }

//wyciaganie dlugosci z wiersza pliku polar.txt
float parse_length(string input){
    string temp;
    int i=0;

    while(input[i]!=' ')
        {
            i++;
        }

    i++;
    while(input[i]!=' ')
        {
            temp+=input[i];
            i++;
        }
    return stof(temp);
}

//wyciaganie kata 'z' wzgledem 'xy' z wiersza pliku polar.txt
float parse_angle_z(string input){
        string temp;
        int i=0;

        while(input[i]!=' ')
            {
                i++;
            }

        i++;
        while(input[i]!=' ')
            {
                i++;
            }

        i++;
        while(input[i]!=NULL)
            {
                temp+=input[i];
                i++;
            }

        return stof(temp);
    }

//wyciaganie poszczegolnych wspolrzednych [x][y][z] z wiersza pliku wspolrzedne.txt
float* parse_xyz(string input, int dimension_num){
    string temp_x;
    string temp_y;
    string temp_z;
    static float xyz[3]={0};

    int i=0;

    //zabezpieczenie przed dziwna liczba wymiarow
    if((dimension_num<2)||(dimension_num>3))
    {
        return xyz;
    }

        //wyciaganie [x]
        while(input[i]!=' ')
        {
            temp_x+=input[i];
            i++;
        }
        //cout<<"[inputX] "<<temp_x<<"[]"<<endl;
        xyz[0]=stof(temp_x);

        //wyciaganie [y]
        i++;
        while(input[i]!=' ')
        {
            temp_y+=input[i];
            i++;
        }
        //cout<<"[inputY] "<<temp_x<<"[]"<<endl;
        xyz[1]=stof(temp_y);

        //wyciaganie [z]
        i++;
        if(dimension_num==3){
            while(input[i]!=NULL)
            {
                temp_z+=input[i];
                i++;
            }
            xyz[2]=stof(temp_z);
        }

    return xyz;
}
