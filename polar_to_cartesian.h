//konwersja wspolrzednych polarnych na kartezjanskie

#define pi 3.14159
#include<math.h>
//zwraca znormalizowana koordynate x
float polar2D_to_cartesian_x(float angle, float length, float max_range_x){
    double x_coordinate;
    angle=(angle/180)*pi;
    x_coordinate=length*cos(angle);

        if(x_coordinate<=max_range_x)
        {
            if(x_coordinate<0.001)
                return 0;
            return x_coordinate/max_range_x;
        }
        else
            return max_range_x;
    }

//zwraca znormalizowana koordynate y
float polar2D_to_cartesian_y(float angle, float length, float max_range_y){
    double y_coordinate;
    angle=(angle/180)*pi;
    y_coordinate=length*sin(angle);

        if(y_coordinate<=max_range_y)
        {
            if(y_coordinate<0.001)
                return 0;
            return y_coordinate/max_range_y;
        }
        else
           return max_range_y;
    }

float polar3D_to_cartesian_x(float angle, float angle_z, float length, float max_range_x){
    double x_coordinate;
    angle=(angle/180)*pi;
    angle_z=(angle_z/180)*pi;
    x_coordinate=length*cos(angle)*cos(angle_z);

        if(x_coordinate<=max_range_x)
        {
            if(x_coordinate<0.001)
                return 0;
            return x_coordinate/max_range_x;
        }
        else
            return max_range_x;
    }

//zwraca znormalizowana koordynate y
float polar3D_to_cartesian_y(float angle, float angle_z, float length, float max_range_y){
    double y_coordinate;
    angle=(angle/180)*pi;
    angle_z=(angle_z/180)*pi;
    y_coordinate=length*sin(angle)*cos(angle_z);

        if(y_coordinate<=max_range_y)
        {
            if(y_coordinate<0.001)
                return 0;
            return y_coordinate/max_range_y;
        }
        else
           return max_range_y;
    }
//zwraca znormalizowana koordynate z
float polar3D_to_cartesian_z(float angle_z, float length, float max_range_z){
    double z_coordinate;
    angle_z=(angle_z/180)*pi;

    z_coordinate=length*sin(angle_z);

        if(z_coordinate<=max_range_z)
        {
            if(z_coordinate<0.001)
                return 0;
            return z_coordinate/max_range_z;
        }

        else
            return max_range_z;
    }

