//tworzenie mapy wektorow z wspolrzednych w ukladzie kartezjanskim
#include<fstream>
#include<parse.h>
#include<polar_to_cartesian.h>
#include<iostream>

//utworzenie pliku zawierajacego wspolrzedne w ukladzie kartezjanskim na
//podstawie pliku z wspolrzedne w ukladzie polarnym
void CREATE_FILE_polar_to_cartesian(string polar_file_name, string cartesian_file_name){
    string temp, temp_x, temp_y;
    float temp_length, temp_angle, temp_angle_z;
    string dim_test;
    int length=1;

    ifstream polar_file;
    ofstream cartesian_file;

    polar_file.open(polar_file_name.c_str());
    getline(polar_file, dim_test);
    polar_file.close();
    for(unsigned int i=0; i<dim_test.size(); i++)
        {
            if(dim_test[i]==' ')
                length++;
        }

    polar_file.open(polar_file_name.c_str());
    cartesian_file.open(cartesian_file_name.c_str());

    //dla 3 wymiarow
    if(length==3)
    {
        while(!polar_file.eof())
        {
            getline(polar_file, temp);

            if(temp.size()<5)   //minimalny rozmiar linii dla 3 wymiarow
                break;
            //wyciaganie koordynatow kartezjanskich z polarnych
            temp_angle=parse_angle(temp);
            temp_length=parse_length(temp);
            temp_angle_z=parse_angle_z(temp);

            //wpisywanie koordynatow do pliku
            cartesian_file << polar3D_to_cartesian_x(temp_angle, temp_angle_z, temp_length, 100) << " " << polar3D_to_cartesian_y(temp_angle, temp_angle_z, temp_length, 100) << " " <<polar3D_to_cartesian_z(temp_angle_z, temp_length, 100) << endl;
        }
    }

    //dla 2 wymiarow
    else
    {
        while(!polar_file.eof())
        {
            getline(polar_file, temp);

            if(temp.size()<3)   //minimalny rozmiar linii dla 2 wymiarow
                break;
            //wyciaganie koordynatow kartezjanskich z polarnych

            temp_angle=parse_angle(temp);
            temp_length=parse_length(temp);

            //wpisywanie koordynatow do pliku
            cartesian_file << polar2D_to_cartesian_x(temp_angle, temp_length, 100) << " " << polar2D_to_cartesian_y(temp_angle, temp_length, 100) << endl;
        }
    }

    polar_file.close();
    cartesian_file.close();

}


float vector_x(float start_x, float end_x){     //obliczanie dlugosci skladowej wektora [x]
    return (end_x-start_x);
}


float vector_y(float start_y, float end_y){     //obliczanie dlugosci skladowej wektora [y]
    return (end_y-start_y);
}


float vector_z(float start_z, float end_z){     //obliczanie dlugosci skladowej wektora [z]
    return (end_z-start_z);

}

float Vector(float start, float end){     //obliczanie dlugosci skladowej wektora [z]
    std::cout<<"[S]"<<start<<"  [E]"<<end<<std::endl;
    return (end-start);

}

void CREATE_FILE_vector_map(string cartesian_file_name, string vector_map_file_name){       //utworzenie pliku zawierajacego mape wektorow na podstawie
                                                                                            //wspolrzednych w ukladzie kartezjanskim
    string temp;

    ifstream cartesian_file;
    ofstream vector_map_file;
    cartesian_file.open(cartesian_file_name.c_str());
    vector_map_file.open(vector_map_file_name.c_str());

    float* start_tab_xyz;
    float* end_tab_xyz;

    float local_start_tab_xyz[3];
    float local_end_tab_xyz[3];

    float var_vector_x;
    float var_vector_y;
    float var_vector_z;


    getline(cartesian_file, temp);

    //wyznaczanie poczatku wektora
    start_tab_xyz=parse_xyz(temp,3);
    local_start_tab_xyz[0]=*start_tab_xyz;
    local_start_tab_xyz[1]=*(start_tab_xyz+1);
    local_start_tab_xyz[2]=*(start_tab_xyz+2);

    while(!cartesian_file.eof())
        {
            getline(cartesian_file, temp);

            if(!cartesian_file.eof())
            {
            end_tab_xyz=parse_xyz(temp,3);
            local_end_tab_xyz[0]=*end_tab_xyz;
            local_end_tab_xyz[1]=*(end_tab_xyz+1);
            local_end_tab_xyz[2]=*(end_tab_xyz+2);


            //obliczanie wektorow
            var_vector_x=vector_x(local_start_tab_xyz[0], local_end_tab_xyz[0]);
            var_vector_y=vector_y(local_start_tab_xyz[1], local_end_tab_xyz[1]);
            var_vector_z=vector_z(local_start_tab_xyz[2], local_end_tab_xyz[2]);

            //wpisywanie wektorow do pliku
            vector_map_file<<var_vector_x<<" "<<var_vector_y<<" "<<var_vector_z<<endl;

            //koniec starego wektora to poczatek nowego
            local_start_tab_xyz[0]=local_end_tab_xyz[0];
            local_start_tab_xyz[1]=local_end_tab_xyz[1];
            local_start_tab_xyz[2]=local_end_tab_xyz[2];
            }

        }
    vector_map_file.close();
    cartesian_file.close();
}

bool coplanar(float* v1, float* v2, float* v3){
    float iloczyn_wektorowy=v1[0]*v2[1]*v3[2]+v1[1]*v2[2]*v3[0]+v1[2]*v2[0]*v3[1]-v1[2]*v2[1]*v3[0]-v1[0]*v2[2]*v3[1]-v1[1]*v2[0]*v3[2];
    //jesli trzy wektory sa coplanar to zwraca true
         if(iloczyn_wektorowy)
             return false;
         else return true;
}

void tab_create2d(float**& tab, int rowCount, int colCount){

    if(rowCount>0)
    {
        tab= new float*[rowCount];   //tyle powstaje wektorow

        for(int i=0; i<rowCount; i++){
            tab[i]= new float[colCount]; //o takich wymiarach
        }
    }
}

void tab_fill2d(float** tab, int rowCount, int colCount, string file_name){
ifstream wektory_plik;
string temp;
float* xyz_wektory;
wektory_plik.open(file_name.c_str());

    for(int i=0; i<rowCount; i++){
        getline(wektory_plik, temp);
        if(temp.size()>0)
        for(int j=0; j<colCount; j++){
                xyz_wektory=parse_xyz(temp, 3);
                tab[i][j]=xyz_wektory[j];

        }else break;
    }
}

void tab_delete2d(float** tab, int w){

    for(int i=0; i<w; i++){
        delete[]tab[i];
    }
    delete[]tab;
}

int ilosc_linii(string file_name){
    int n=0;
    ifstream plik;
    string temp="1";
    plik.open(file_name.c_str());

            while(temp.size()){
                getline(plik, temp);

                if(temp.size())
                    n++;
            }
            plik.close();
            return n;
}

